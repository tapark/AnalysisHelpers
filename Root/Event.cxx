#include "AnalysisHelpers/Event.h"

#include "TROOT.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODCutFlow/CutBookkeeperAuxContainer.h"

Event::Event(const std::vector<std::string>& inputFiles, const std::string& treeName) :
	m_treeName(treeName),
	m_inputFiles(inputFiles)
{
	ROOT::EnableThreadSafety();
	xAOD::Init().ignore();
}

ana::table::partition Event::allocate()
{
	TDirectory::TContext c;
	ana::table::partition partition;

	long long offset = 0ll;
	unsigned int ipart = 0;
	// check all files for tree clusters
	for (const auto& filePath : m_inputFiles) {

		// check file
		std::unique_ptr<TFile> file(TFile::Open(filePath.c_str()));
		if (!file) {
			continue;
		} else if (file->IsZombie()) {
			continue;
		}

		// check tree
		auto tree = file->Get<TTree>(m_treeName.c_str());
		auto event = std::make_unique<xAOD::TEvent>();
		if (!tree || event->readFrom(tree).isFailure()) {
			continue;
		}

		// get file entries
		long long fileEntries = event->getEntries();
		// add part to partition
		m_goodFiles.push_back(filePath);
		partition.add(ipart++,offset,offset+fileEntries);
		offset += fileEntries;
	}

	return partition;
}

std::shared_ptr<Event::Loop> Event::open(const ana::table::range& part) const 
{
	auto tree = std::make_unique<TChain>(m_treeName.c_str(),m_treeName.c_str());
	for (auto const& filePath : m_goodFiles) {
		tree->Add(filePath.c_str());
	}
	tree->ResetBit(kMustCleanup);
	return std::make_shared<Loop>(part, tree.release());
}

Event::Loop::Loop(const ana::table::range& part, TTree* tree) :
	ana::table::reader<Loop>(part)
{
	m_event = std::make_unique<xAOD::TEvent>();
  if (m_event->readFrom(tree).isFailure()) {
		throw std::runtime_error("failed to read event");
	};
}

void Event::Loop::begin()
{
	m_current = m_part.begin;
	m_end = m_part.end;
}

bool Event::Loop::next()
{
	if (m_current < m_end) {
    if (m_event->getEntry(m_current++)<0) {
			throw std::runtime_error("failed to get entry");
		} 
		return true;
  }
  return false;
}