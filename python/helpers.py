import ROOT

import sys
import collections
import glob
import numpy as np

def vectorize(l,dtype=None):
	if not dtype:
		# cast elements to common denominator type
		l = np.array(l).tolist()
		dtype = type(l[0])
	vector = ROOT.std.vector(dtype)()
	for ele in l:
		vector.push_back(ele)
	return vector

def binning(axis='x',**kwargs):
	isUniform = all(['n{}'.format(axis) in kwargs, '{}min'.format(axis) in kwargs, '{}max'.format(axis) in kwargs])
	isColumn = '{}bins'.format(axis) in kwargs
	if not (isUniform or isColumn):
		raise ValueError("Failed to get bins for {ax}-axis: neither (n,min,max) or [bins] provided.".format(ax=axis))
	elif isUniform:
		return vectorize(np.linspace(kwargs['{}min'.format(axis)],kwargs['{}max'.format(axis)],kwargs['n{}'.format(axis)]+1),dtype=ROOT.double)
	elif isColumn:
		return vectorize(kwargs['{}bins'.format(axis)],dtype=ROOT.double)