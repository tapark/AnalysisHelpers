# key:value modules
import collections
import toml
import json
# string processing
import six
import string

def merge(d1, d2):
	"""
	Modifies d1 in-place to contain values from d2.  If any value
	in d1 is a dictionary (or dict-like), *and* the corresponding
	value in d2 is also a dictionary, then merge them in-place.
	"""
	for k, v2 in d2.items():
		v1 = d1.get(k)  # returns None if v1 has no value for this key
		if (isinstance(v1, collections.Mapping) and
			isinstance(v2, collections.Mapping)):
			merge(v1, v2)
		else:
			d1[k] = v2

def encode(d):
	"""
	Create a new dict where all text strings are ensured to be encoded
	"""
	encoded = collections.OrderedDict()
	for k, v in d.items():
		k = encode_str(k)
		v = encode_str(v)
		encoded[k] = v
	return encoded

def encode_str(v):
	"""
	Encode any and all strings found in argument
	"""
	if isinstance(v,six.text_type): return v.encode()
	elif isinstance(v,(list,tuple)): return [encode_str(ele) for ele in v]
	elif isinstance(v,collections.Mapping): return encode(v)
	else: return v

def read(files):
	"""
	Read TOML configuration file(s)
	- keys:value ordering is preserved
	- sections with the same name are merged
	"""
	if not files: return collections.OrderedDict()
	if not isinstance(files,(list,tuple)): files = [files]	
	cfg = collections.OrderedDict()
	for f in files:
		try:
			kvs = toml.load(f,_dict=collections.OrderedDict)
		except toml.decoder.TomlDecodeError as err:
			raise err
		except IOError as err:
			raise err
		merge(cfg,kvs)	
	return encode(cfg)

def patch(d,tags):
	patched = collections.OrderedDict()
	for k,v in d.items():
		k = patch_str(k,tags)
		v = patch_str(v,tags)
		patched[k] = v
	return patched

def patch_str(v,tags):
	if isinstance(v,six.string_types): return string.Template(v).substitute(tags)
	elif isinstance(v,(list,tuple)): return [patch_str(ele,tags) for ele in v]
	elif isinstance(v,collections.Mapping): return patch(v,tags)
	else: return v
	
def dumps(cfg):
	"""
	Dump config as a JSON expression
	"""
	return json.dumps(cfg)