import os
import string
import collections
import warnings
import fnmatch
import importlib
import functools
import copy
import types
import subprocess

# inspect
try:
    from inspect import getfullargspec as get_args
except ImportError:
    from inspect import getargspec as get_args

import ROOT

from .helpers import vectorize, binning
from .cling import jit_function, get_typename

terminal_width = 240
try:
	terminal_width = int(subprocess.check_output(['stty', 'size']).decode().split()[1])
except:
	pass

def multithread(concurrency=-1):
	ROOT.ana.multithread.enable(concurrency)

class Analysis:
	"""
	Python interface for ana::analysis
	"""	

	def __init__(self,*args,**kwargs):
		self._analysis = ROOT.ana.analysis(kwargs['dataset'])(kwargs.get('entries',-1))
		self._analysis.scale(float(kwargs.get('weight',1.0)))
		self._analysis.open(*args)
		# store nodes
		self.columns   = {}
		self.selections  = {}
		self.counters = {}

	def open(self,*args):	
		cppargs = []
		for arg in args:
			if isinstance(arg,(list,tuple)):
				cppargs.append(vectorize(arg))
			else:
				process_args.append(arg)
		self._analysis.open(*cppargs)

	# ---------------------------------------------------------------------------
	# observables
	# ---------------------------------------------------------------------------

	def read(self,dtype):
		def read(name,*args):
			tmpl_pars = tuple([dtype] + [type(arg) for arg in args])
			var = self._analysis.read[tmpl_pars](name,*args)
			self.columns[name] = var
			return var
		return read

	def constant(self,name,value):
		var = self._analysis.constant[type(value)](name,value)
		self.columns[name] = var
		return var

	def define(self, definition):
		def define(name, *arguments):
			"""
			define a variable

			Parameters
			----------
			definition : C++ class (name)
				class of variable
			name :
				column name
			arguments : unpacked
				constructor arguments

			Returns
			-------
			var : node<definition>
				variable node
			"""
			# define variable
			tmpl_pars = tuple([definition] + [type(arg) for arg in arguments])
			var = self._analysis.define[tmpl_pars](name,*arguments)
			self.columns[name] = var
			return var
		return define

	def evaluate(self,name,expression):
		"""
		evaluate an expression

		Parameters
		----------
		name : str
			name of equation

		Returns
		-------
		var
			equation node
		"""
		# get variable name/node/types
		termNames = ROOT.str.tokenize_expression(expression,self._analysis.list_term_names())
		terms = [self.columns[varName] for varName in termNames]
		termTypes = ["decltype(std::declval<typename {}::node_type>().value())".format(type(term).__cpp_name__) for term in terms]

		# jit expression
		lmbd = jit_function(termTypes, termNames, expression)

		# evaluate equation
		var = self._analysis.evaluate(name,lmbd,*terms)
		self.columns[name] = var
		return var

		# warnings.filterwarnings(RuntimeWarning)  # supress warnings
		# warnings.filterwarnings("default")  # restore warnings

	# ---------------------------------------------------------------------------
	# selections
	# ---------------------------------------------------------------------------

	def at(self,rebase):
		self._analysis.at(rebase)
		return self

	def filter(self,name,cut=None,weight=None,channel=False):
		"""
		apply a cut

		Parameters
		----------
		snippet : callable
			python function that returns a C++ expression to compile

		Returns
		-------
		flt
			filter node
		"""
		# get variable types

		if cut and weight:
			raise RuntimeError("filter must be either cut or weight")
		elif cut:
			expression = cut
			termNames = ROOT.str.tokenize_expression(expression,self._analysis.list_term_names())
			terms = [self.columns[varName] for varName in termNames]
			termTypes = ["decltype(std::declval<typename {}::node_type>().value())".format(type(term).__cpp_name__) for term in terms]
			lmbd = jit_function(termTypes, termNames, expression)
			tmplPars = tuple([ROOT.ana.selection.cut] + [type(lmbd).__cpp_name__]+ [type(term).node_type for term in terms])
			flt = self._analysis.filter[tmplPars](name,lmbd,*terms) if not channel else self._analysis.channel[tmplPars](name,lmbd,*terms)
		elif weight:
			expression = weight
			termNames = ROOT.str.tokenize_expression(expression,self._analysis.list_term_names())
			terms = [self.columns[varName] for varName in termNames]
			termTypes = ["decltype(std::declval<typename {}::node_type>().value())".format(type(term).__cpp_name__) for term in terms]
			lmbd = jit_function(termTypes, termNames, expression)
			tmplPars = tuple([ROOT.ana.selection.weight] + [type(lmbd).__cpp_name__]+ [type(term).node_type for term in terms])
			flt = self._analysis.filter[tmplPars](name,lmbd,*terms) if not channel else self._analysis.channel[tmplPars](name,lmbd,*terms)
		else:
			raise RuntimeError("filter must be either cut or weight")
		# monkey patch node's filter method to accept C++ expressions
		def _filter(node,name,cut=None,weight=None,channel=False):
			return self.at(node).filter(name,cut=cut,weight=weight,channel=channel)
		# flt.filter = types.MethodType(_filter, flt, type(flt))
		flt.filter = types.MethodType(_filter, flt)
		return flt

	# ---------------------------------------------------------------------------
	# include snippet
	# ---------------------------------------------------------------------------

	def include(self,snippet):
		def _include(*args,**kwargs):
			return snippet(self,*args,**kwargs)
		return _include

	# ---------------------------------------------------------------------------
	# counters
	# ---------------------------------------------------------------------------

	def count(self,counter):
		def count(name,*args):
			tmpl_pars = tuple([counter] + [type(arg) for arg in args])
			cnt = self._analysis.count[tmpl_pars](name,*args)
			return cnt
		return count

	def histogram(self,dim,dtype):
		def histogram(name,**kwargs):
			if not any([dim==1,dim==2,dim==3]):
				raise ValueError("unsupported dimension = {}".format(dim))
			xbins = binning('x',**kwargs)
			if dim>=2:
				ybins = binning('y',**kwargs)
				if dim==3:
					zbins = binning('z',**kwargs)
					return self.count('Histogram<{},{}>'.format(dim,dtype))(name,xbins,ybins,zbins)
				else:
					return self.count('Histogram<{},{}>'.format(dim,dtype))(name,xbins,ybins)
			else:
				return self.count('Histogram<{},{}>'.format(dim,dtype))(name,xbins)
		return histogram

	def profile(self,dim,raw=False,scale=1.0):
		def profile(name,**kwargs):
			xbins = binning('x',**kwargs)
			if (dim==1):
				return self.count(ROOT.Profile(dim))(name,xbins)
			elif (dim==2):
				ybins = binning('y',**kwargs)
				return self.count(ROOT.Profile(dim))(name,xbins,ybins)
			else:
				raise ValueError("unsupported dimension = {}".format(dim))
		return profile

	def scan(self,dim,dtype,raw=False,scale=1.0):
		def scan(name,**kwargs):
			if dim==1:
				nx, xmin, xmax = kwargs['nx'], kwargs['xmin'], kwargs['xmax']
				ybins = binning('y',**kwargs)
				return self.count(ROOT.Scan(dim,dtype))(name,nx,xmin,xmax,ybins)
			elif dim==2:
				nx, xmin, xmax = kwargs['nx'], kwargs['xmin'], kwargs['xmax']
				ny, ymin, ymax = kwargs['ny'], kwargs['ymin'], kwargs['ymax']
				zbins = binning('z',**kwargs)
				return self.count(ROOT.Scan(dim,dtype))(name,nx,xmin,xmax,ny,ymin,ymax,zbins)
			else:
				raise ValueError("unsupported dimension = {}".format(dim))
		return scan

# ---------------------------------------------------------------------------
# apply blocks on analysis
# ---------------------------------------------------------------------------

def compute(ana,constants={},columns={},definitions={}):
	print("="*terminal_width)
	tableTitle = '{name:<{w}.{w}} {klass:<{w}.{w}} {expr:<{w}.{w}} {py:<{w}.{w}}'.format(w=terminal_width/4-1,name='Column',klass='Type',expr='Expression/Arguments',py='Snippet.py')
	print(tableTitle)
	print("-"*terminal_width)

	for name,action in constants.items():
		val = action['val']
		try:
			var = ana.constant(name,val)
		except Exception as exception:
			raise RuntimeError("Failed to set constant '{}': {}",name,str(exception))
		tableLine = '{name:<{w}.{w}} {klass:<{w}.{w}} {expr:<{w}.{w}} {snip:<{w}.{w}}'.format(w=terminal_width/4-1,name=name,klass=str(type(val)),expr=branch,snip='')
		print(tableLine)

	for name,action in columns.items():
		dtype, branch = action['dtype'], action.get('branch',name)
		try:
			var = ana.read(dtype)(name,branch)
		except Exception as exception:
			raise RuntimeError("Failed to read column '{}': {}",name,str(exception))
		tableLine = '{name:<{w}.{w}} {klass:<{w}.{w}} {expr:<{w}.{w}} {snip:<{w}.{w}}'.format(w=terminal_width/4-1,name=name,klass=dtype,expr=branch,snip='')
		print(tableLine)

	if isinstance(definitions,list): definitions = collections.OrderedDict(definitions)
	for name,action in definitions.items():
		# jitted equation
		if isinstance(action,str):
			expression = action
			try:
				var = ana.evaluate(name, expression)
			except Exception as exception:
				raise RuntimeError("Failed to evaluate variable '{} = {}': {}".format(name,expression,str(exception)))
			tableLine = '{name:<{w}.{w}} {klass:<{w}.{w}} {expr:<{w}.{w}} {snip:<{w}.{w}}'.format(w=terminal_width/4-1,name=name,klass="equation",expr=expression,snip='')
			print(tableLine)
		# compiled definition
		else:
			definition, observables, include = action['def'], action.get('obs',[]), action.get('py','')
			# try:
			var = ana.define(definition)(name)
			args = [ana.columns[argName] for argName in observables]
			var.evaluate(*args)
			if include:
				module = importlib.import_module(include)
				snippet = getattr(module,name)
				snippet(ana)
			# except Exception as exception:
			# 	raise RuntimeError("Failed to define variable '{} = {}({})': {}".format(name,definition,', '.join(args),str(exception)))
			tableLine = '{name:<{w}.{w}} {klass:<{w}.{w}} {expr:<{w}.{w}} {snip:<{w}.{w}}'.format(w=terminal_width/4-1,name=name,klass=definition,expr='('+', '.join(observables)+')',snip=include)
			print(tableLine)

def filter(ana,selections):
	print("="*terminal_width)
	tableTitle = '{name:<{w}.{w}} {klass:<{w}.{w}} {expr:<{w}.{w}} {prev:<{w}.{w}}'.format(w=terminal_width/4-1,name='Selection',klass='Type',expr='Expression',prev='@ Previous')
	print(tableTitle)
	print("-"*terminal_width)

	# function to select selections matching pattern
	def match(patterns):
		all_selection_paths = ana._analysis.list_selection_paths()
		matched_selection_paths = []
		for pattern in patterns:
			for matched_selection_path in fnmatch.filter(all_selection_paths,pattern):
				if not matched_selection_path in matched_selection_paths: matched_selection_paths.append(matched_selection_path)
		matched_selections = []
		for matched_selection_path in matched_selection_paths:
			matched_selections.append(ana._analysis[matched_selection_path])
		return matched_selections

	if isinstance(selections,list): selections = collections.OrderedDict(selections)
	for name,action in selections.items():
		# check if cut or weight
		cut, wgt = 'cut' in action, 'weight' in action
		expression = action['cut'] if cut else action['weight']
		channel = action.get('channel',False)
		prevs = match(action.get('at',[]))

		if not (cut or wgt):
			raise ValueError("Failed to apply filter '{} = {}': must be either cut or weight (neither provided).".format(name,expression))
		if cut and wgt:
			raise ValueError("Failed to apply filter '{} = {}': must be either cut or weight (cannot be both).".format(name,expression))

		try:
			flts = []
			if not prevs:
				flt = ana.filter(name,cut=expression,channel=channel) if cut else ana.filter(name,weight=expression,channel=channel)
				flts.append(flt)
			else:
				for prev in prevs:
					flt = ana.at(prev).filter(name,cut=expression,channel=channel) if cut else ana.at(prev).filter(name,weight=expression,channel=channel)
					flts.append(flt)
		except Exception as exception:
			raise RuntimeError("Failed to apply filter '{} = {}': {}".format(name,expression,str(exception)))

		for flt in flts:
			tableLine = '{name:<{w}.{w}} {klass:<{w}.{w}} {expr:<{w}.{w}} {prev:<{w}.{w}}'.format(w=terminal_width/4-1,name=flt.model().path(),klass=get_typename(flt.model(),concise=True),expr=expression,prev='' if flt.model().is_initial() else flt.model().get_previous().path())
			print(tableLine)

def count(ana,cutflows={},histograms={},profiles={},scans={},efficiencies={}):
	print("="*terminal_width)
	tableTitle = '{name:<{w}.{w}} {klass:<{w}.{w}} {obs:<{w}.{w}} {flts:>{w}}'.format(w=terminal_width/4-1,name="Counter",klass="Type",flts="# of selections",obs="Observable(s)")
	print(tableTitle)
	print("-"*terminal_width)

	selected_counters = []

	def book(cnt,patterns):
		all_selection_paths = ana._analysis.list_selection_paths()
		matched_selection_paths = []
		for pattern in patterns:
			matched_selection_paths.extend(fnmatch.filter(all_selection_paths,pattern))
		matched_selection_paths = sorted(list(set(matched_selection_paths)))
		matched_selections = [ana._analysis[path] for path in matched_selection_paths]
		booked_counters = []
		for flt in matched_selections:
			booked_counters.append(flt.book(cnt))
		return list(zip(matched_selections, booked_counters))

	for name,action in histograms.items():
		raw = action.get('raw',False)
		scale = action.get('scale',1.0)
		dim, dtype = action['dim'], action['dtype']
		try:
			hist = ana.histogram(dim,dtype)(name,**action)
		except Exception as exception:
			raise RuntimeError("Failed to make histogram<{},{}> '{}': {}".format(dim,dtype,name,str(exception)))

		# fill observables
		xobs = action['xobs']
		fills = [xobs]
		if dim>=2:
			yobs = action['yobs']
			fills.append(yobs)
		if dim==3:
			zobs = action['zobs']
			fills.append(zobs)
		fills = map(list, zip(*fills))
		for observables in fills:
			observables = [ana.columns[obs] for obs in observables]
			hist.fill(*observables)

		# book selections
		booked_hists = book(hist,action['at'])
		selected_counters.extend(booked_hists)
		tableLine = '{name:<{w}.{w}} {klass:<{w}.{w}} {obs:<{w}.{w}} {flts:>{w}}'.format(w=terminal_width/4-1,name=name,klass='Histogram<{},{}>'.format(dim,dtype),flts=len(booked_hists),obs=str(fills))
		print(tableLine)

	for name,action in profiles.items():
		raw = action.get('raw',False)
		scale = action.get('scale',1.0)
		dim  = action['dim']
		try:
			prof = ana.profile(dim)(name,**action)
		except Exception as exception:
			raise RuntimeError("Failed to make <{},{}>-profile '{}': {}".format(dim,dtype,name,str(exception)))

		# fill observables
		xobs = action['xobs']
		fills = [xobs]
		yobs = action['yobs']
		fills.append(yobs)
		if dim==2:
			zobs = action['zobs']
			fills.append(zobs)
		fills = map(list, zip(*fills))
		for observables in fills:
			observables = [ana.columns[obs] for obs in observables]
			prof.fill(*observables)

		# book selections
		booked_profs = book(prof,action['at'])
		selected_counters.extend(booked_profs)

		tableLine = '{name:<{w}.{w}} {klass:<{w}.{w}} {obs:<{w}.{w}} {flts:>{w}}'.format(w=terminal_width/4-1,name=name,klass='Profile<{},{}>'.format(dim,dtype),flts=len(booked_profs),obs=str(fills))
		print(tableLine)

	for name,action in scans.items():
		raw = action.get('raw',False)
		scale = action.get('scale',1.0)
		dim, dtype = action['dim'], action['dtype']
		try:
			scan = ana.scan(dim,dtype)(name,**action)
		except Exception as exception:
			raise RuntimeError("Failed to make <{},{}>-scan '{}': {}".format(dim,dtype,name,str(exception)))

		# fill observables
		if dim==1:
			fills = action['yobs']
		elif dim==2:
			fills = action['zobs']
		[ scan.fill(ana.columns[obs]) for obs in fills ]

		# book selections
		booked_scans = book(scan,action['at'])
		selected_counters.extend(booked_scans)

		tableLine = '{name:<{w}.{w}} {klass:<{w}.{w}} {obs:<{w}.{w}} {flts:>{w}}'.format(w=terminal_width/4-1,name=name,klass='Scan<{},{}>'.format(dim,dtype),flts=len(booked_scans),obs=str(fills))
		print(tableLine)

	return selected_counters

def definition(klass=None, observables=[]):

	if callable(klass):
		# decorator w/o arguments
		# accept an existing variable node,
		# apply patch function on each slot
		@functools.wraps(klass)
		def wrapper(ana,*args,**kwargs):
			varName = klass.__name__
			var = ana.columns[varName]
			# klass(var.model(),*args,**kwargs)
			klass(var.model())
			for i in range(var.concurrency()):
				# klass(var.slot(i),*args,**kwargs)
				klass(var.slot(i))
		return wrapper

	else:
		# decorator(klass, observables)
		# define variable node with function name
		# apply patch function on each slot
		def decorator(func):
			@functools.wraps(func)
			def wrapper(ana,*args,**kwargs):
				varName = func.__name__
				args = [ana.columns[argName] for argName in observables]
				var = ana.define(klass)(varName,[args])
				# apply snippet on slots
				func(var.model(),*args,**kwargs)
				for i in range(var.concurrency()):
					func(var.slot(i),*args,**kwargs)
				return var
			return wrapper
		return decorator

def equation(func):
	@functools.wraps(func)
	def wrapper(ana,*args,**kwargs):
		# get variable name
		varName = func.__name__
		# resolve expression
		expression = string.Template(func(*args,**kwargs)).substitute(kwargs)
		# evaluate equation
		eqn = ana.evaluate(varName,expression)
		return eqn
	return wrapper

def cut(func=None, at='', channel=False):
	def decorator(func):
		# wrapper needs analysis instance
		@functools.wraps(func)
		def wrapper(ana,*args,**kwargs):
			# get filter name
			flt_name = func.__name__
			# resolve expression
			expression = string.Template(func(*args,**kwargs)).substitute(kwargs)
			# rebase filter (if set)
			if at:
				ana._analysis.at(ana._analysis[at])
			# apply filter
			cut = ana.filter(flt_name,cut=expression,channel=channel)
			return cut
		return wrapper
	# if no parentheses, apply at current filter as non-channel
	if callable(func):
		return decorator(func)
	else:
		return decorator

def weight(func=None, at='', channel=False):
	def decorator(func):
		@functools.wraps(func)
		def wrapper(ana,*args,**kwargs):
			# get filter name
			flt_name = func.__name__
			# resolve expression
			expression = string.Template(func(*args,**kwargs)).substitute(kwargs)
			# rebase filter (if set)
			if at:
				ana._analysis.at(ana._analysis[at])
			# apply weight
			wgt = ana.filter(flt_name, weight=expression,channel=channel)
			return wgt
		return wrapper
	# if no parentheses, apply at current filter as non-channel
	if callable(func):
		return decorator(func)
	else:
		return decorator
