# AnalysisHelpers

## Setup

```
mkdir MyAnalysis && cd MyAnalysis/
git clone https://github.com/taehyounpark/ana.git
git clone ssh://git@gitlab.cern.ch:7999/tapark/AnalysisHelpers.git
setupATLAS
asetup AnalysisBase,21.2.210,here

cd ../ && mkdir build && cd build/
setupATLAS
cmake ../MyAnalysis/
cmake --build
source */setup.sh
```

## Quick start

### TTree analysis
```
import ROOT
from AnalysisHelpers import analysis

ana = analysis.Analysis('treeAnalysis', dataset='Tree')
ana.open("AntiKt4EMPFlow",["file.tree.root"])

# read branches
# analysis actions are lazy -- they will be executed per-event only when needed
ana.read('int')('nJets', "njet")
ana.read('ROOT::RVec<float>')("allJetsPt", "jet_pT")
```

### xAOD analysis
```
import ROOT
from AnalysisHelpers import analysis

# run on 10 cores
analysis.multithread(10)

# Event = xAOD::TEvent inputs
ana = analysis.Analysis('myAnalysis',dataset='Event')
ana.open(['sample.DAOD_TRUTH3.root'])

# analysis actions are lazy
eventInfo = ana.read['xAOD::EventInfo']('eventInfo', "EventInfo")
allElectrons = ana.read('xAOD::ElectronContainer)

# valid C++ expressions are just-in-time compiled
diElectronMass = ana.evaluate('mee','(allElectrons[0]->p4() + allElectrons[1]->p4()).M()/1000')

# cuts and weights can be applied as filters
mcEventWeight = ana.filter('mcEventWeight', weight="eventInfo.mcEventWeight()")
twoElectronsCut = ana.filter('exactly2Electrons', cut="allElectrons.size() == 2")

# histograms/etc. can be filled with variables & booked at filter(s)
diElectronMassHist = ana.histogram(1,float)('diElectronMass', nx=100,xmin=0,xmax=1000)
diElectronMassHist.fill(diElectronMass)
diElectronMassHist.book(twoElectronsCut)

# retriving a result triggers the analysis
diElectronMassHist['exactly2Electrons'].result()  # TH1F
```

### Configurize the analysis: `analyze.py`
```
$ analyze.py --help
```