#pragma once

#include <string>
#include <vector>
#include <utility>
#include <memory>

#include <ROOT/RVec.hxx>
#include <ROOT/RDataSource.hxx>

#include "ana/table.h"

class RDS : public ana::table::dataset<RDS>
{

private:
  using RDataSource   = ROOT::RDF::RDataSource;

public:
	class Reader;

	template <typename T>
	class Column;

public:
	RDS(std::unique_ptr<RDataSource>);
	virtual ~RDS() = default;

	virtual ana::table::partition allocate() override;
	std::shared_ptr<Reader> open(const ana::table::range& part) const;

	virtual void start() override;
	virtual void finish() override;

protected:
  std::unique_ptr<RDataSource> m_rds;

};

class RDS::Reader : public ana::table::reader<Reader>
{

public:
	Reader(const ana::table::range& part, RDataSource& rds);
	~Reader() = default;

	template <typename T>
	std::shared_ptr<Column<T>> read(const std::string& name) const;

	virtual void begin() override;
	virtual bool next() override;
	virtual void end() override;

protected:
  ana::table::range m_part;
  RDataSource*      m_rds;
  long long         m_current;

};

template <typename T>
class RDS::Column : public ana::column<T>
{

public:
	Column(const std::string& name, T** cursor) :
		ana::column<T>(name),
		m_cursor(cursor)
	{}
	~Column() = default;

	virtual const T& value() const override
	{
		return static_cast<const T&>(**m_cursor);
	}

protected:
	T** m_cursor;

};

template <typename T>
std::shared_ptr<RDS::Column<T>> RDS::Reader::read(const std::string& name) const
{
  auto columnReaders = m_rds->GetColumnReaders<T>(name.c_str());
	return std::make_shared<Column<T>>(name,columnReaders[m_part.slot]);
}
