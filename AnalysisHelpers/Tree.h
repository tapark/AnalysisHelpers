#pragma once

#include <string>
#include <vector>
#include <memory>

#include <ROOT/RVec.hxx>

#include "TDirectory.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TTreeReaderArray.h"

#include "ana/table.h"
#include "ana/reader.h"

class Tree : public ana::table::dataset<Tree>
{

public:
	class Reader;

	template <typename T>
	class Branch;

public:
	Tree(const std::string& treeName, const std::vector<std::string>& inputFiles);
	virtual ~Tree() = default;

	virtual ana::table::partition allocate() override;
	std::shared_ptr<Reader> open(const ana::table::range& part) const;

protected:
	std::string m_treeName;
	std::vector<std::string> m_inputFiles;
	std::vector<std::string> m_goodFiles;

};

class Tree::Reader : public ana::table::reader<Reader>
{
public:
	Reader(const ana::table::range& part, std::unique_ptr<TTree> tree);
	~Reader() = default;

	template <typename U>
	std::shared_ptr<Branch<U>> read(const std::string& columnName, std::string branchName="") const;

	virtual void begin() override;
	virtual bool next() override;

protected:
	std::unique_ptr<TTree>       m_tree; 
	std::unique_ptr<TTreeReader> m_treeReader; 

};

template <typename T>
class Tree::Branch : public ana::column<T>::reader
{

public:
	Branch(const std::string& columnName, const std::string& branchName, TTreeReader& treeReader) :
		ana::column<T>::reader(columnName),
		m_branchName(branchName),
		m_treeReader(&treeReader)
	{}
	~Branch() = default;

	virtual void initialize() override
	{
		m_treeReaderValue = std::make_unique<TTreeReaderValue<T>>(*m_treeReader,this->m_branchName.c_str());
	}

	virtual void execute() override
	{
		this->read(**m_treeReaderValue);
	}

protected:
  std::string m_branchName;
	TTreeReader* m_treeReader;
  std::unique_ptr<TTreeReaderValue<T>> m_treeReaderValue;

};

template <typename T>
class Tree::Branch<ROOT::RVec<T>> : public ana::column<ROOT::RVec<T>>::reader
{

public:
	Branch(const std::string& columnName, const std::string& branchName, TTreeReader& treeReader) :
		ana::column<ROOT::RVec<T>>::reader(columnName),
		m_branchName(branchName),
		m_treeReader(&treeReader)
	{}
	~Branch() = default;

	virtual void initialize() override
	{
		m_treeReaderArray = std::make_unique<TTreeReaderArray<T>>(*m_treeReader,this->m_branchName.c_str());
	}

	virtual void execute() override
	{
    if (auto arraySize = m_treeReaderArray->GetSize()) {
      ROOT::RVec<T> readArray(&m_treeReaderArray->At(0), arraySize);
      std::swap(m_readArray,readArray);
    } else {
      ROOT::RVec<T> emptyVector{};
      std::swap(m_readArray,emptyVector);
    }
		this->read(m_readArray);
	}

protected:
  std::string m_branchName;
	TTreeReader* m_treeReader;
  std::unique_ptr<TTreeReaderArray<T>> m_treeReaderArray;
	mutable ROOT::RVec<T> m_readArray;

};

template <>
class Tree::Branch<ROOT::RVec<bool>> : public ana::column<ROOT::RVec<bool>>::reader
{

public:
	Branch(const std::string& columnName, const std::string& branchName, TTreeReader& treeReader) :
		ana::column<ROOT::RVec<bool>>::reader(columnName),
		m_branchName(branchName),
		m_treeReader(&treeReader)
	{}
	~Branch() = default;

	virtual void initialize() override
	{
		m_treeReaderArray = std::make_unique<TTreeReaderArray<bool>>(*m_treeReader,this->m_branchName.c_str());
	}

	virtual void execute() override
	{
    if (m_treeReaderArray->GetSize()) {
      ROOT::RVec<bool> readArray(m_treeReaderArray->begin(), m_treeReaderArray->end());
      std::swap(m_readArray,readArray);
    } else {
      ROOT::RVec<bool> emptyVector{};
      std::swap(m_readArray,emptyVector);
    }
		this->read(m_readArray);
	}

protected:
  std::string m_branchName;
	TTreeReader* m_treeReader;
  std::unique_ptr<TTreeReaderArray<bool>> m_treeReaderArray;
	mutable ROOT::RVec<bool> m_readArray;

};

template <typename U>
std::shared_ptr<Tree::Branch<U>> Tree::Reader::read(const std::string& columnName, std::string branchName) const
{
	if (branchName.empty()) branchName = columnName;
	return std::make_shared<Branch<U>>(columnName,branchName,*m_treeReader);
}