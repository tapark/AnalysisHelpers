import ROOT
from AnalysisHelpers import analysis

files = ROOT.std.vector('std::string')()
files += ["/scratch/thpark/DijetInsituJER/data/DijetTree/mc/gridOutput/rawDownload/user.tapark.DijetTree_mc16a_Pythia8_JZ3WithSW_20220130_1241_tree.root/user.tapark.27983235._000002.tree.root"]

ana = analysis.Analysis("AntiKt4EMPFlow",files,entries=1000000,data="Tree")