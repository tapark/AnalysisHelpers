#include <iostream>
#include <vector>
#include <string>
#include <memory>

#include <ROOT/RVec.hxx>
#include "TROOT.h"
#include "TDirectory.h"
#include "TPad.h"

#include "ana/sample.h"
#include "ana/analysis.h"
#include "ana/column.h"
#include "ana/selection.h"
#include "ana/counter.h"
#include "AnalysisHelpers/Tree.h"
#include "AnalysisHelpers/Histogram.h"

int main(int argc, char** argv) {

  // ------------------------------------
  // ANALYSIS
  // ------------------------------------  

  // enable multithreading
  ana::multithread::enable(7);

  // only run over the first 1M entries
  auto ana = ana::analysis<Tree>(1000000);

  // open dataset
  ana.open(
    std::string("AntiKt4EMPFlow"),
    std::vector<std::string>({"/scratch/thpark/DijetInsituJER/data/DijetTree/mc/gridOutput/rawDownload/user.tapark.DijetTree_mc16a_Pythia8_JZ3WithSW_20220130_1241_tree.root/user.tapark.27983235._000002.tree.root","/scratch/thpark/DijetInsituJER/data/DijetTree/mc/gridOutput/rawDownload/user.tapark.DijetTree_mc16a_Pythia8_JZ3WithSW_20220130_1241_tree.root/user.tapark.27983235._000008.tree.root"})
  );

  // ------------------------------------
  // COMPUTE VARIABLES
  // ------------------------------------
  
  // choose columns to read from the data
  auto mcEventWeight = ana.read<float>("mcEventWeight","mcEventWeight");
  auto nJets = ana.read<int>("nJets","njet");
  auto jetsPt = ana.read<ROOT::RVec<float>>("jetsPt","jet_pT");
  auto jetsPtTrue = ana.read<ROOT::RVec<float>>("jetsPtTrue","jet_pT_true");

  // evaluate arbitrary transformations of existing columns
  // - return type is deduced from the supplied lambda
  // - argument types are automatically converted between input columns & lambda arguments
  auto jetsPtResponse = ana.evaluate("jetsPtResponse", [](const ROOT::RVec<float>& jetsPt, const ROOT::RVec<float>& jetsPtTrue){ return jetsPt/jetsPtTrue; }, jetsPt, jetsPtTrue);

  // define a custom column accepting existing columns as arguments
  // auto myTriggerSelection = ana.define<MyTriggerSelection>("triggerSelection", passedTriggers);

  // useful to bring in external data & configuration into computation graph
  // std::vector<std::string> myTriggers = {"HLT_j15","HLT_j25"};
  // myTriggerSelection.apply( [=]( MyTriggerSelection& trigSel) { trigSel.setTriggers(myTriggers); } );
  // ------------------------------------

  // ------------------------------------
  // APPLY SELECTIONS
  // ------------------------------------

  // filter is applied as either a "cut" or "weight"
  auto inclusive = ana.filter<ana::selection::cut>("inclusive",[](){return true;});  // cut = true , weight = 1.0
  // applying another filter automatically compounds the previous
  auto eventWeight = ana.filter<ana::selection::weight>("eventWeight",[](float mcEventWeight){ return mcEventWeight; }, mcEventWeight);  // cut = true && true, weight = 1.0 * mcEventWeight

  // selections can be applied "from" an existing one to branch out...
  auto noJets = ana.at(eventWeight).channel<ana::selection::cut>("noJets",[](int nJets){return nJets==0;}, nJets);  // cut = true && true && nJet<2, weight = eventWeight * 1.0 * 1.0
  // ... or directly on it...
  auto oneJet = eventWeight.channel<ana::selection::cut>("oneJet",[](int nJets){return nJets==1;}, nJets);  // cut = true && true && nJet<2, weight = eventWeight * 1.0 * 1.0
  // ... which can also be accessed from main analysis graph.
  auto twoOrMoreJetsCut = ana["eventWeight"].channel<ana::selection::cut>("twoOrMoreJets",[](int nJets){return nJets>=2;}, nJets);  // cut = true && true && nJet>=2, weight = eventWeight * 1.0 * 1.0
  // (branches need not be orthogonal)

  // the "full path" of a selection gives the list of selection names applied up to and including.
  std::cout << twoOrMoreJetsCut.full_path() << std::endl;
  // selections are saved in terms of their "flattened paths"
  std::cout << twoOrMoreJetsCut.path() << std::endl;

  // a filter can be called as "channel" to force downstream selection paths to be nested
  auto leadingJetPt20 = ana.filter<ana::selection::cut>("leadingJetPt20", [](const ROOT::RVec<float>& jetsPt){return (jetsPt>20).size();}, jetsPt);
  std::cout << leadingJetPt20.full_path() << std::endl;
  std::cout << leadingJetPt20.path() << std::endl;

  // ------------------------------------
  // BOOK COUNTERS
  // ------------------------------------

  // 1. making a counter is bookkeeper for each filter
  auto nJetsHist = ana.count<Histogram<1,float>>("nJets",vec::make_lin(10,0,10));

  // 2. fill it with columns
  nJetsHist.fill(nJets);

  // 3. booking a bookkeeper counter at a filter instantiates the counter
  auto nJetsHistAtInclusiveCut = inclusive.book(nJetsHist);
  // booking can be invoked the other way
  auto nJetsHistAtLeast2Jets = nJetsHist.book(twoOrMoreJetsCut);
  // (just don't do both for a specific filter/counter pair)

  // ------------------------------------
  // GET RESULTS
  // ------------------------------------

  // retrieving the result of a counter triggers the analysis run
  std::cout << nJetsHistAtInclusiveCut.result()->GetMean() << std::endl;

  // concrete counters are accessible at the site of mgr.book(selection), or through mgr["selection path"]
  std::cout << (nJetsHistAtInclusiveCut.result() == nJetsHist["inclusive"].result()) << std::endl;

  // results can be retrieved multiple times
  std::cout << nJetsHist["inclusive"].result()->GetMean() << std::endl;

  return 0;
}