#!/usr/bin/env python

import os
import collections
import resource
import subprocess
import numpy as np

def main(args):

	# ----------------------------------------------------------------------------
	# debug mode
	# ----------------------------------------------------------------------------
	# - disable multithreading
	# - process 100 entries
	# - force output to debug.root
	if args.debug:
		args.mt = 0
		args.entries = 100
		args.output = 'debug.root'
		args.force = True

	analyze(args)

def analyze(args):

	import ROOT

	terminal_width = 240
	try:
		terminal_width = int(subprocess.check_output(['stty', 'size']).decode().split()[1])
	except:
		pass

	from AnalysisHelpers import analysis
	from AnalysisHelpers import config
	from AnalysisHelpers import helpers

	# ----------------------------------------------------------------------------
	# enable/disable multi-threading
	# ----------------------------------------------------------------------------
	analysis.multithread(args.mt)

	# ensure output directory
	outputPath = os.path.abspath(args.output)
	outputDir = os.path.dirname(outputPath)
	if not os.path.isdir(outputDir):
		os.path.makedirs(outputDir)

	# make output file (overwrite if neeeded)
	if os.path.isfile(outputPath):
		if args.force:
			print("overwriting output file: {}".format(outputPath))
		else:
			raise RuntimeError("output file already exists: {}".format(outputPath))
	else:
		print("writing output file: {}".format(outputPath))

	# ----------------------------------------------------------------------------
	# open sample
	# ----------------------------------------------------------------------------
	ana = analysis.Analysis(args.tree,helpers.vectorize(args.files,dtype='string'),data="Tree",weight=args.weight,entries=args.entries)

	# ----------------------------------------------------------------------------
	# patch tags
	# ----------------------------------------------------------------------------
	patchList = args.patch[::-1]
	tagsCfg = config.read( args.tags )
	tags = collections.OrderedDict()
	for patch in patchList:
		tags.update(tagsCfg.get(patch,{}))

	if tags:
		print("")
		print("="*terminal_width)
		tableTitle = '{tag:<{w}.{w}} {ptch:<{w}.{w}} {val:<{w2}.{w2}}'.format(w=terminal_width/4-1,w2=terminal_width/2-1,tag="Tag",ptch="Patch",val="Value")
		print(tableTitle)
		print("-"*terminal_width)
		for patchedTag,patchedVal in tags.items():
			prioritizedPatch = patchList[-1]
			for patch in patchList:
				if patchedTag in tagsCfg.get(patch,{}):
					prioritizedPatch = patch
			tableLine = '{tag:<{w}.{w}} {ptch:<{w}.{w}} {val:<{w2}.{w2}}'.format(w=terminal_width/4-1,w2=terminal_width/2-1,tag="${{{}}}".format(patchedTag),ptch=prioritizedPatch,val=config.dumps(patchedVal) if isinstance(patchedVal,collections.Mapping) else str(patchedVal))
			print(tableLine)

	# ----------------------------------------------------------------------------
	# compute, filter, count
	# ----------------------------------------------------------------------------
	observablesCfg = config.read( args.compute )
	observablesCfg = config.patch(observablesCfg,tags)
	constants = observablesCfg.get('constants',{})
	columns = observablesCfg.get('columns',{})
	definitions = observablesCfg.get('definitions',{})
	includes = observablesCfg.get('include',{})
	analysis.compute(ana,constants=constants,columns=columns,definitions=definitions)

	selectionsCfg = config.read( args.filter )
	selections = config.patch(selectionsCfg,tags)
	analysis.filter(ana,selections=selections)

	countersCfg = config.read( args.count )
	cutflows = countersCfg.get('cutflows',{})
	histograms = countersCfg.get('histograms',{})
	profiles = countersCfg.get('profiles',{})
	scans = countersCfg.get('scans',{})
	selected_counters = analysis.count(ana,cutflows=cutflows,histograms=histograms,profiles=profiles,scans=scans)

	print("-"*terminal_width)

	# ----------------------------------------------------------------------------
	# run & output results
	# ----------------------------------------------------------------------------
	print("")
	# output results to folder
	print("analyzing sample: {}/{}".format(args.analysis,args.sample))
	outputFile = ROOT.TFile.Open(outputPath,'recreate' if args.force else 'create')
	sampleFolder = outputFile.GetDirectory(os.path.join(args.analysis,args.sample))
	if not sampleFolder: sampleFolder = outputFile.mkdir(os.path.join(args.analysis,args.sample))
	sampleFolder = outputFile.GetDirectory(os.path.join(args.analysis,args.sample))
	for selection,counter in selected_counters:
		selectionFolder = sampleFolder.GetDirectory(selection.path())
		if not selectionFolder: selectionFolder= sampleFolder.mkdir(selection.path())
		selectionFolder = sampleFolder.GetDirectory(selection.path())
		selectionFolder.WriteObject(counter.result[''](),counter.name())
	# write folders to file
	print("all done. max(RSS) = {:.1f} MB".format(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000))
	print("")

if __name__ == "__main__":

	# ----------------------------------------------------------------------------
	# parse arguments
	# ----------------------------------------------------------------------------

	import argparse
	parser = argparse.ArgumentParser(description="run analysis on sample")

	parser.add_argument('sample',     type=str,   metavar='name', default='data', help='sample name')

	parser.add_argument('--mt',      type=int,   metavar='cpus', default=1, help='multi-threading concurrency (0: single-threaded)')
	parser.add_argument('--entries', type=int,   default=-1,       help='maximum number of entries to run (-1: all entries)')

	parser.add_argument('--input',   type=str,   metavar='input.cfg', required=True, help='sample input configuration')
	parser.add_argument('--weight',  type=float, default=1.0,      help='output file path')

	parser.add_argument('--analysis', type=str, default='samples',  help='select a sample to run')
	parser.add_argument('--compute',  type=str, nargs='+',          help='compute columns')
	parser.add_argument('--filter',   type=str, nargs='+',          help='apply selections')
	parser.add_argument('--count',    type=str, nargs='+',          help='book counters')

	parser.add_argument('--patch', type=str, nargs='+', metavar='tags.cfg', help='patch tags')

	parser.add_argument('--output',   type=str, metavar='analyzed.root', default='analyzed.root', help='output file path')
	parser.add_argument('--force',    action='store_true',                                        help='overwrite output file')
	parser.add_argument('--debug',    action='store_true', help='run in debug mode')

	args = parser.parse_args()


	# ----------------------------------------------------------------------------
	# run program
	# ----------------------------------------------------------------------------
	main(args)