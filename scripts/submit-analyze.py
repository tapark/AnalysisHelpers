#!/usr/bin/env python
import os
import collections
import glob
import numpy as np

def main(args):
	from AnalysisHelpers import config

	# main config
	mainCfg = config.read(args.config)

	# [analyze]
	analyzeCfg = mainCfg['analyze']
	submitCfg = mainCfg.get('submit',{})

	# analysis name
	analysisName = analyzeCfg.get('analysis','analysis')
	# observables dir + cfgs
	computeDir   = analyzeCfg['observables'].get('dir','./')
	computeList  = [ os.path.join(computeDir,fileName+'.cfg') for fileName in analyzeCfg['observables']['cfg'] ]
	# filters dir + cfgs
	filterDir    = analyzeCfg['selections'].get('dir','./')
	filterList   = [ os.path.join(filterDir,fileName+'.cfg') for fileName in analyzeCfg['selections']['cfg'] ]
	# counters dir + cfgs
	countDir     = analyzeCfg['counters'].get('dir','./')
	countList    = [ os.path.join(countDir,fileName+'.cfg') for fileName in analyzeCfg['counters']['cfg'] ]
	# tags dir + cfgs
	tagsDir  = analyzeCfg['tags'].get('dir','./')
	tagsList = [ os.path.join(tagsDir,fileName+'.cfg') for fileName in analyzeCfg['tags']['cfg'] ]

	# [submit]
	campaignsCfg = config.read(analyzeCfg['campaigns']['cfg'])
	samplesMap   = config.read(analyzeCfg['samples']['map'])
	jobsCfg = config.read( submitCfg.get('jobs',{}).get('cfg',{}) )

	# campaign
	campaignName = args.campaign

	# make tasks
	tasks = []

	dataCfg = config.read( campaignsCfg[campaignName].get('data',{}).get('cfg','') )
	dataList = dataCfg.get('whitelist',{})
	for dataSample,whitelisted in dataList.items():
		if not whitelisted : continue

		# get arguments
		treeName     = dataCfg['tree'].get(dataSample,'')
		samplePath   = samplesMap['path'].get(dataSample,dataSample)
		patchList    = dataCfg.get('patch',{}).get(dataSample,[])

		# split files into parts
		allFiles    = [ os.path.abspath(filePath) for filePath in sorted(set(glob.glob(*dataCfg['files'].get(dataSample,[])))) ]
		sampleSplit = len(allFiles) if args.split=='max' or int(args.split)<0 else int(args.split)
		sampleSplit = jobsCfg.get(dataSample,{}).get('parts',sampleSplit)
		fileLists   = np.array_split( allFiles, max(1,sampleSplit) )

		# submit for each sub-list
		for ipart, fileList in enumerate(fileLists):
			if not len(fileList): continue
			# make task
			if sampleSplit:
				taskName = '{cmp}_{smp}_{ana}_{id}_part{pt}'.format(cmp=campaignName, smp=dataSample, ana=analysisName, id=args.identifier, pt=ipart+1)
			else:
				taskName = '{cmp}_{smp}_{ana}_{id}'.format(cmp=campaignName, smp=dataSample, ana=analysisName, id=args.identifier)
			taskOutput = os.path.join(taskName+'.root')
			taskCmd = 'analyze.py' 
			taskCmd += ' --tree {}'.format(treeName)
			taskCmd += ' --files {}'.format(' '.join(fileList))
			taskCmd += ' --sample {}'.format(samplePath)
			taskCmd += ' --analysis {}'.format(analysisName)
			taskCmd += ' --compute {}'.format(' '.join(computeList))
			taskCmd += ' --filter {}'.format(' '.join(filterList))
			taskCmd += ' --count {}'.format(' '.join(countList))
			taskCmd += ' --patch {}'.format(' '.join(patchList))
			taskCmd += ' --tags {}'.format(' '.join(tagsList))
			taskCmd += ' --output {}'.format(taskOutput)
			taskCmd += ' --mt {}'.format(jobsCfg.get(dataSample,{}).get('cpus',args.ncores))
			task = submit.task(
				taskName,
				taskCmd,
				ncores=jobsCfg.get(dataSample,{}).get('cpus',args.ncores),
				memory=jobsCfg.get(dataSample,{}).get('mem',args.memory),
				time=jobsCfg.get(dataSample,{}).get('time',args.time),
				setup=submit.getSetupCommand(
					args,
					# HACK: only works for AnalysisBase
					ANALYSIS_BUILD_DIR=os.environ.get('TestArea')
				),
				outputs=[taskOutput]
			)
			tasks.append(task)
			if args.check:
				print(taskCmd)

	mcCfg   = config.read( campaignsCfg[campaignName].get('mc',{}).get('cfg','') ) 
	mcList = mcCfg.get('whitelist',{})
	for mcSample,whitelisted in mcList.items():
		if not whitelisted : continue

		# get arguments
		treeName     = mcCfg['tree'].get(mcSample,'')
		samplePath   = samplesMap['path'].get(mcSample,mcSample)
		scale = campaignsCfg[campaignName].get('lumi',1.0) * np.prod(samplesMap.get('xsec',{}).get(mcSample,[1.0])) / mcCfg.get('norm',{}).get(mcSample,1.0)
		patchList    = mcCfg.get('patch',{}).get(mcSample,[])

		# split files into parts
		allFiles    = [ os.path.abspath(filePath) for filePath in sorted(set(glob.glob(*mcCfg['files'].get(mcSample,[])))) ]
		sampleSplit = len(allFiles) if args.split=='max' or int(args.split)<0 else int(args.split)
		sampleSplit = jobsCfg.get(mcSample,{}).get('parts',sampleSplit)
		fileLists   = np.array_split( allFiles, max(1,sampleSplit) )

		# submit for each sub-list
		for ipart, fileList in enumerate(fileLists):
			if not len(fileList): continue
			# make task
			if sampleSplit:
				taskName = '{cmp}_{smp}_{ana}_{id}_part{pt}'.format(cmp=campaignName, smp=mcSample, ana=analysisName, id=args.identifier, pt=ipart+1)
			else:
				taskName = '{cmp}_{smp}_{ana}_{id}'.format(cmp=campaignName, smp=mcSample, ana=analysisName, id=args.identifier)
			taskOutput = os.path.join(taskName+'.root')
			taskCmd = 'analyze.py' 
			taskCmd += ' --tree {}'.format(treeName)
			taskCmd += ' --files {}'.format(' '.join(fileList))
			taskCmd += ' --sample {}'.format(samplePath)
			taskCmd += ' --weight {}'.format(scale)
			taskCmd += ' --analysis {}'.format(analysisName)
			taskCmd += ' --compute {}'.format(' '.join(computeList))
			taskCmd += ' --filter {}'.format(' '.join(filterList))
			taskCmd += ' --count {}'.format(' '.join(countList))
			taskCmd += ' --patch {}'.format(' '.join(patchList))
			taskCmd += ' --tags {}'.format(' '.join(tagsList))
			taskCmd += ' --output {}'.format(taskOutput)
			taskCmd += ' --mt {}'.format(jobsCfg.get(mcSample,{}).get('cpus',args.ncores))
			task = submit.task(
				taskName,
				taskCmd,
				ncores=jobsCfg.get(mcSample,{}).get('cpus',args.ncores),
				memory=jobsCfg.get(mcSample,{}).get('mem',args.memory),
				time=jobsCfg.get(mcSample,{}).get('time',args.time),
				setup=submit.getSetupCommand(
					args,
					# HACK: only works for AnalysisBase
					ANALYSIS_BUILD_DIR=os.environ.get('TestArea')
				),
				outputs=[taskOutput]
			)
			tasks.append(task)
			if args.check:
				print(taskCmd)

	if not args.check:
		ctrl = submit.guessSubmissionController(args)
		ctrl.submitTasks(args,tasks)

if __name__ == "__main__":

	import argparse
	# add script arguments
	parser = argparse.ArgumentParser(description="script to submit analyze.py jobs")
	parser.add_argument('config',      type=str, metavar='analyze.cfg', help='analyze.py configuration file')
	parser.add_argument('--campaign',  type=str, metavar='campaign',      help='campaign to run over')
	parser.add_argument('--parts',     dest='split', type=str, default='0', help='split job files into N parts')
	parser.add_argument('--check',     action='store_true', help='check commands')
	parser.add_argument('--local',     action='store_true', help='check commands')

	# add SH arguments
	from SubmissionHelpers import submit
	submit.configureDefaultArgumentParser(parser)

	# parse arguments
	args = parser.parse_args()

	# HACK: only works for AnalysisBase
	args.setup.append(os.path.join(os.environ.get('TestArea'),'x86_64-centos7-gcc8-opt/setup.sh'))

	# run main program
	main(args)